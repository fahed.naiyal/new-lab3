#ifndef IMAGE_ROTATION_TRANSFORM_H
#define IMAGE_ROTATION_TRANSFORM_H

#include "image.h"

struct image rotateImage(struct image const source);

#endif 
