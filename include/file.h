#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H
#include "stdio.h"
enum state {
  WORKED = 0,
  ERROR_OPEN,
  ERROR_CLOSE,
  ERROR_READ,
  ERROR_WRITE_2,
  ERROR_CONVERT
};

void freeFile(FILE **f1, FILE **f2);

enum state openFile(FILE **f, char const *path, char const *mode);

enum state closeFile(FILE *f);

#endif
